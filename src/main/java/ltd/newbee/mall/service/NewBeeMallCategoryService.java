package ltd.newbee.mall.service;


import ltd.newbee.mall.entity.GoodsCategory;
import ltd.newbee.mall.util.PageQueryUtil;
import ltd.newbee.mall.util.PageResult;
import ltd.newbee.mall.vo.NewBeeMallIndexCategoryVO;
import ltd.newbee.mall.vo.SearchPageCategoryVO;

import java.util.List;
import java.util.Objects;

/*
 *
 * 本接口是分类管理模块,共有4个方法getCategoriesForIndex
 * 分别是商品类目分页列表接口,添加商品类目接口,修改商品类目接口,批量删除商品类目接口
 * */
public interface NewBeeMallCategoryService {

    //后台分页
    PageResult getCategoryPage(PageQueryUtil pageQueryUtil);

    //新增分类
    String saveCategory(GoodsCategory goodsCategory);

    //分类更新
    String updateGoodsCategory(GoodsCategory goodsCategory);

    //获取分类列表
    GoodsCategory getCategoryById(Long categoryId);

    //删除分类(支持批量删除)
    Boolean deleteBatch(Integer[] ids);

    //通过分类等级,上层id和数量
    List<GoodsCategory> selectByLevelAndParentIdsAndNumber(List<Long> parentIds, int categoryLevel);

    List<NewBeeMallIndexCategoryVO> getCategoriesForIndex();

    SearchPageCategoryVO getCategoriesForSearch(Long categoryId);
}
