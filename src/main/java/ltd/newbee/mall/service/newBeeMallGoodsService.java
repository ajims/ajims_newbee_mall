package ltd.newbee.mall.service;

import ltd.newbee.mall.entity.NewBeeMallGoods;
import ltd.newbee.mall.util.PageQueryUtil;
import ltd.newbee.mall.util.PageResult;


public interface newBeeMallGoodsService {
    //新商品添加
    String saveNewBeeMallGoods(NewBeeMallGoods newBeeMallGoods);

    NewBeeMallGoods getNewBeeMallGoodsById(Long goodsId);

    //原有商品修改
    String updateNewBeeMallGoods(NewBeeMallGoods newBeeMallGoods);

    //获取商品页初始页面
    PageResult getNewBeeMallGoodsPage(PageQueryUtil pageQueryUtil);

    //修改上下架状态
    int batchUpdateGoodsSellStatus(Long[] ids,int sellStatus);

    /**
     * 商品搜索
     * @return
     */
    PageResult searchNewBeeMallGoods(PageQueryUtil pageQueryUtil);

}
