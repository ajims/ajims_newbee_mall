package ltd.newbee.mall.service;


import ltd.newbee.mall.entity.Carousel;
import ltd.newbee.mall.util.PageQueryUtil;
import ltd.newbee.mall.util.PageResult;
import ltd.newbee.mall.vo.NewBeeMallIndexCarouselVO;

import java.util.List;

/*
*轮播图分页列表接口
*添加轮播图接口
*根据 id 获取单条轮播图记录接口
*修改轮播图接口
*批量删除轮播图接口
* */
public interface NewBeeMallCarouselService {

    //后台分页
    PageResult getCarouselPage(PageQueryUtil pageQueryUtil);

    //轮播图添加
    String saveCarousel(Carousel carousel);

    //轮播图修改
    String updateCarousel(Carousel carousel);


    Carousel getCarouselById(Integer carouselId);

    //删除轮播图
    Boolean deleteBatch(Integer[] ids);

    List<NewBeeMallIndexCarouselVO> getCarouselsForIndex(int number);
}
