package ltd.newbee.mall.service;

import ltd.newbee.mall.entity.AdminUser;

public interface AdminUserService {
    AdminUser login(String userName,String passWord);

    //获取详细名称信息
    AdminUser getUserDetailById(Integer loginUserId);

    //更新密码
    boolean updatePassword(int loginUserId,String originalPassword,String newPassword);

    boolean updateName(Integer loginUserId,String loginUserName,String nickName);
}
