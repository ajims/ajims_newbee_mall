package ltd.newbee.mall.service;

import ltd.newbee.mall.dao.MallUserMapper;
import ltd.newbee.mall.entity.MallUser;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.beans.Transient;

@Service
public class TransactionTestService {

    @Resource
    MallUserMapper mallUserMapper;

    public Boolean test1(){
        MallUser user = new MallUser();
        user.setPasswordMd5("123456");
        user.setLoginName("test1");

        mallUserMapper.insertSelective(user);

        System.out.println(1/0);
        return true;
    }

    @Transactional
    public Boolean test2(){
        MallUser user = new MallUser();
        user.setPasswordMd5("123456");
        user.setLoginName("test1");

        mallUserMapper.insertSelective(user);

        System.out.println(1/0);
        return true;
    }
}
