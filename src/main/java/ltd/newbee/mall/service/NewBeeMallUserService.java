package ltd.newbee.mall.service;

import ltd.newbee.mall.entity.MallUser;
import org.apache.ibatis.annotations.Param;

import javax.servlet.http.HttpSession;
import java.util.Objects;

//前端注册,登录用接口
public interface NewBeeMallUserService {

    String register(String loginName,String password);
    
    String login(String loginName, String password, HttpSession session);
}
