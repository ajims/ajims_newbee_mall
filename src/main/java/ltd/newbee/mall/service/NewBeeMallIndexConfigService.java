package ltd.newbee.mall.service;

import ltd.newbee.mall.entity.IndexConfig;
import ltd.newbee.mall.util.PageQueryUtil;
import ltd.newbee.mall.util.PageResult;
import ltd.newbee.mall.vo.NewBeeMallIndexConfigGoodsVO;

import java.util.List;

//首页配置管理模块接口设计及实现
public interface NewBeeMallIndexConfigService {

    //1.首页配置分页列表接口
    PageResult getConfigsPage(PageQueryUtil pageQueryUtil);

    //2.添加首页配置接口
    String saveIndexConfig(IndexConfig config);

    //3.修改首页配置接口
    String updateConfig(IndexConfig config);

    //4.批量删除首页配置接口
    Boolean deleteBatch(Long[] ids);

    //5.首页配置分页的详情
    IndexConfig getIndexConfigById(Long id);

    /**
     * 返回固定数量的首页配置商品对象(首页调用)
     *
     * @param number
     * @return
     */
    List<NewBeeMallIndexConfigGoodsVO> getConfigGoodsesForIndex(int configType, int number);

}
