package ltd.newbee.mall.service.Impl;

import ltd.newbee.mall.common.Constants;
import ltd.newbee.mall.dao.NewBeeMallGoodsMapper;
import ltd.newbee.mall.dao.newBeeMallShoppingCartItemMapper;
import ltd.newbee.mall.entity.NewBeeMallGoods;
import ltd.newbee.mall.entity.newbeeMallShoppingCartItem;
import ltd.newbee.mall.enums.ServiceResultEnum;
import ltd.newbee.mall.service.newBeeMallShoppingCartService;
import ltd.newbee.mall.util.BeanUtil;
import ltd.newbee.mall.vo.NewBeeMallShoppingCartItemVO;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
public class newBeeMallShoppingCartServiceImpl implements newBeeMallShoppingCartService {

    @Resource
    private newBeeMallShoppingCartItemMapper newBeeMallShoppingCartItemMapper;

    @Resource
    private NewBeeMallGoodsMapper newBeeMallGoodsMapper;

    @Override
    public String saveNewBeeMallCartItem(newbeeMallShoppingCartItem newbeeMallShoppingCartItem) {
        //1.查询数据库,2.判断是否为空
        newbeeMallShoppingCartItem tmp = newBeeMallShoppingCartItemMapper
                .selectByUserIdAndGoodsId(newbeeMallShoppingCartItem.getUserId(), newbeeMallShoppingCartItem.getGoodsId());

        //如果购物车已经有了这种商品,就改变其商品量
        if (tmp != null) {
            //?
            tmp.setGoodsCount(tmp.getGoodsCount() + newbeeMallShoppingCartItem.getGoodsCount());
            return updateNewBeeMallCartItem(tmp);
        }

        NewBeeMallGoods newBeeMallGoods = newBeeMallGoodsMapper.selectByPrimaryKey(newbeeMallShoppingCartItem.getGoodsId());
        //商品为空
        if (newBeeMallGoods == null) {
            return ServiceResultEnum.GOODS_NOT_EXIST.getResult();
        }

        int totalItem = newBeeMallShoppingCartItemMapper.selectCountByUserId(newbeeMallShoppingCartItem.getUserId());
        //超出最大数量
        if (totalItem > Constants.SHOPPING_CART_ITEM_LIMIT_NUMBER) {
            return ServiceResultEnum.SHOPPING_CART_ITEM_LIMIT_NUMBER_ERROR.getResult();
        }

        //保存记录
        if (newBeeMallShoppingCartItemMapper.insertSelective(newbeeMallShoppingCartItem) > 0) {
            return ServiceResultEnum.SUCCESS.getResult();
        }
        return ServiceResultEnum.DB_ERROR.getResult();
    }

    @Override
    public String updateNewBeeMallCartItem(newbeeMallShoppingCartItem newBeeMallShoppingCartItem) {
        newbeeMallShoppingCartItem newBeeMallShoppingCartItemUpdate =
                newBeeMallShoppingCartItemMapper.selectByPrimaryKey(newBeeMallShoppingCartItem.getCartItemId());

        if (newBeeMallShoppingCartItemUpdate == null) {
            return ServiceResultEnum.DATA_NOT_EXIST.getResult();
        }

        //超出最大数量
        if (newBeeMallShoppingCartItem.getGoodsCount() > Constants.SHOPPING_CART_ITEM_LIMIT_NUMBER) {
            return ServiceResultEnum.SHOPPING_CART_ITEM_LIMIT_NUMBER_ERROR.getResult();
        }

        newBeeMallShoppingCartItemUpdate.setGoodsCount(newBeeMallShoppingCartItem.getGoodsCount());
        newBeeMallShoppingCartItemUpdate.setUpdateTime(new Date());
        //修改记录
        if (newBeeMallShoppingCartItemMapper.updateByPrimaryKeySelective(newBeeMallShoppingCartItemUpdate) > 0) {
            return ServiceResultEnum.SUCCESS.getResult();
        }
        return ServiceResultEnum.DB_ERROR.getResult();
    }

    @Override
    public List<NewBeeMallShoppingCartItemVO> getMyShoppingCartItems(Long newBeeMallUserId) {
        List<NewBeeMallShoppingCartItemVO> newBeeMallShoppingCartItemVOS = new ArrayList<>();
        //根据userId查询所有的购物项数据
        List<newbeeMallShoppingCartItem> newBeeMallShoppingCartItems = newBeeMallShoppingCartItemMapper
                .selectByUserId(newBeeMallUserId, Constants.SHOPPING_CART_ITEM_TOTAL_NUMBER);

        if (!CollectionUtils.isEmpty(newBeeMallShoppingCartItems)) {
            //获取商品id
            List<Long> newBeeMallGoodsIds = newBeeMallShoppingCartItems
                    .stream().map(newbeeMallShoppingCartItem::getGoodsId).collect(Collectors.toList());
            //查询商品信息并做数据转,
            List<NewBeeMallGoods> newBeeMallGoods = newBeeMallGoodsMapper.selectByPrimaryKeys(newBeeMallGoodsIds);
            Map<Long, NewBeeMallGoods> newBeeMallGoodsMap = new HashMap<>();
            //如何把两个list的数据放进一个map中
            if (!CollectionUtils.isEmpty(newBeeMallGoods)) {
                //toMap(key,value,冲突值);    (goodsId,newBeeMallGoods)作为一个值
                //Function.identity()返回一个输出跟输入一样的Lambda表达式对象，等价于形如 t -> t形式的Lambda表达式。
                //https://www.jianshu.com/p/267c53dd4295
                //第三个参数是为了如果发生冲突,应选择entity1作为值
                newBeeMallGoodsMap = newBeeMallGoods.stream().collect(Collectors
                        .toMap(NewBeeMallGoods::getGoodsId, Function.identity(), (entity1, entity2) -> entity1));
            }
            //封装返回数据
            for (newbeeMallShoppingCartItem newBeeMallShoppingCartItem : newBeeMallShoppingCartItems) {
                NewBeeMallShoppingCartItemVO newBeeMallShoppingCartItemVO = new NewBeeMallShoppingCartItemVO();
                BeanUtil.copyProperties(newBeeMallShoppingCartItem, newBeeMallShoppingCartItemVO);
                if (newBeeMallGoodsMap.containsKey(newBeeMallShoppingCartItem.getGoodsId())) {
                    NewBeeMallGoods tmpBeeMallGoods = newBeeMallGoodsMap.get(newBeeMallShoppingCartItem.getGoodsId());
                    newBeeMallShoppingCartItemVO.setGoodsCoverImg(tmpBeeMallGoods.getGoodsCoverImg());
                    String goodsName = tmpBeeMallGoods.getGoodsName();

                    if (goodsName.length() > 28) {
                        goodsName = goodsName.substring(0, 28) + "";
                    }

                    newBeeMallShoppingCartItemVO.setGoodsName(goodsName);
                    newBeeMallShoppingCartItemVO.setSellingPrice(tmpBeeMallGoods.getSellingPrice());
                    newBeeMallShoppingCartItemVOS.add(newBeeMallShoppingCartItemVO);

                }
            }
        }
        return newBeeMallShoppingCartItemVOS;
    }

    @Override
    public Boolean deleteById(Long newBeeMallShoppingCartItemId) {
        //删除成功的条数
        return newBeeMallShoppingCartItemMapper.deleteByPrimaryKey(newBeeMallShoppingCartItemId) > 0;
    }


}
