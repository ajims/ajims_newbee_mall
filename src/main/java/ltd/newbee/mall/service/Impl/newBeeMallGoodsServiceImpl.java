package ltd.newbee.mall.service.Impl;

import ltd.newbee.mall.dao.NewBeeMallGoodsMapper;
import ltd.newbee.mall.entity.NewBeeMallGoods;
import ltd.newbee.mall.enums.ServiceResultEnum;
import ltd.newbee.mall.service.newBeeMallGoodsService;
import ltd.newbee.mall.util.BeanUtil;
import ltd.newbee.mall.util.PageQueryUtil;
import ltd.newbee.mall.util.PageResult;
import ltd.newbee.mall.vo.NewBeeMallSearchGoodsVO;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class newBeeMallGoodsServiceImpl implements newBeeMallGoodsService {

    @Resource
    private NewBeeMallGoodsMapper newBeeMallGoodsMapper;

    @Override
    public String saveNewBeeMallGoods(NewBeeMallGoods newBeeMallGoods) {
        if (newBeeMallGoodsMapper.insertSelective(newBeeMallGoods) > 0) {
            return ServiceResultEnum.SUCCESS.getResult();
        }
        return ServiceResultEnum.DB_ERROR.getResult();

    }

    @Override
    public NewBeeMallGoods getNewBeeMallGoodsById(Long goodsId) {
        return newBeeMallGoodsMapper.selectByPrimaryKey(goodsId);
    }

    @Override
    public String updateNewBeeMallGoods(NewBeeMallGoods newBeeMallGoods) {
        NewBeeMallGoods tmp = newBeeMallGoodsMapper.selectByPrimaryKey(newBeeMallGoods.getGoodsId());
        //todo 为什么要tmp变量,2.既然newBeeMallGoods永远不为空,那为什么还要判断
        if (tmp == null) {
            return ServiceResultEnum.DATA_NOT_EXIST.getResult();
        }

        newBeeMallGoods.setUpdateTime(new Date());
        if (newBeeMallGoodsMapper.updateByPrimaryKeySelective(newBeeMallGoods) > 0) {
            return ServiceResultEnum.SUCCESS.getResult();
        }
        return ServiceResultEnum.DB_ERROR.getResult();

    }

    @Override
    public PageResult getNewBeeMallGoodsPage(PageQueryUtil pageQueryUtil) {
        List<NewBeeMallGoods> list = newBeeMallGoodsMapper.findNewBeeMallGoodsList(pageQueryUtil);
        int total = newBeeMallGoodsMapper.getTotalNewBeeMallGoods(pageQueryUtil);
        PageResult pageResult = new PageResult(total, pageQueryUtil.getPage(), pageQueryUtil.getLimit(), list);
        return pageResult;
    }

    @Override
    public int batchUpdateGoodsSellStatus(Long[] ids, int sellStatus) {
        return newBeeMallGoodsMapper.batchUpdateSellStatus(ids, sellStatus);
    }


    /*
    * 我们定义了 searchNewBeeMallGoods() 方法并传入 PageQueryUtil 对象作为参数,商品类目 id 、关键字 keyword 字段、分页所需的 page 字段、排序字段等都作为属性放在了这个对象中,
    * 关键字或者商品类目 id 用来过滤想要的商品列表，page 用于确定查询第几页的数据,之后通过 SQL 查询出对应的分页数据，再之后是填充数据,
    * 某些字段太长会导致页面上的展示效果不好，所以对这些字段进行了简单的字符串处理并设置到 NewBeeMallSearchGoodsVO 对象中，最终返回的数据是 PageResult 对象。
    * */
    @Override
    public PageResult searchNewBeeMallGoods(PageQueryUtil pageQueryUtil) {

        List<NewBeeMallGoods> goodsList = newBeeMallGoodsMapper.findNewBeeMallGoodsListBySearch(pageQueryUtil);

        int total = newBeeMallGoodsMapper.getTotalNewBeeMallGoodsBySearch(pageQueryUtil);

        List<NewBeeMallSearchGoodsVO> newBeeMallSearchGoodsVOS = new ArrayList<>();

        if (!CollectionUtils.isEmpty(goodsList)) {
            newBeeMallSearchGoodsVOS = BeanUtil.copyList(goodsList, NewBeeMallSearchGoodsVO.class);

            for (NewBeeMallSearchGoodsVO newBeeMallSearchGoodsVO : newBeeMallSearchGoodsVOS) {
                String tmpGoodsName = newBeeMallSearchGoodsVO.getGoodsName();
                String tmpGoodsIntro = newBeeMallSearchGoodsVO.getGoodsIntro();
                // 字符串过长导致文字超出的问题
                if (tmpGoodsName.length() > 28) {
                    tmpGoodsName = tmpGoodsName.substring(0, 28) + "...";
                    newBeeMallSearchGoodsVO.setGoodsName(tmpGoodsName);
                }
                if (tmpGoodsIntro.length() > 30) {
                    tmpGoodsIntro = tmpGoodsName.substring(0, 30) + "...";
                    newBeeMallSearchGoodsVO.setGoodsIntro(tmpGoodsIntro);
                }

            }
        }

        PageResult pageResult = new PageResult(total, pageQueryUtil.getPage(), pageQueryUtil.getLimit(), newBeeMallSearchGoodsVOS);
        return pageResult;
    }


}
