package ltd.newbee.mall.service.Impl;

import ltd.newbee.mall.common.Constants;
import ltd.newbee.mall.dao.MallUserMapper;
import ltd.newbee.mall.entity.MallUser;
import ltd.newbee.mall.enums.ServiceResultEnum;
import ltd.newbee.mall.service.NewBeeMallUserService;
import ltd.newbee.mall.util.BeanUtil;
import ltd.newbee.mall.util.MD5Util;
import ltd.newbee.mall.vo.NewBeeMallUserVO;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

@Service
public class NewBeeMallUserServiceImpl implements NewBeeMallUserService {

    @Resource
    private MallUserMapper mallUserMapper;

    @Override
    public String register(String loginName, String password) {
        if(mallUserMapper.selectByLoginName(loginName) != null){
            return ServiceResultEnum.SAME_LOGIN_NAME_EXIST.getResult();
        }

        MallUser registerUser = new MallUser();
        registerUser.setLoginName(loginName);
        String passwordMD5 = MD5Util.MD5Encode(password,"UTF-8");
        registerUser.setPasswordMd5(passwordMD5);

        if(mallUserMapper.insertSelective(registerUser)>0){
            return ServiceResultEnum.SUCCESS.getResult();
        }

        return ServiceResultEnum.DB_ERROR.getResult();
    }

    @Override
    public String login(String loginName, String MD5password, HttpSession session) {
        MallUser user = mallUserMapper.selectByLoginNameAndPasswd(loginName,MD5password);

        if(user != null && session != null ){
            if(user.getLockedFlag() == 1){
                return ServiceResultEnum.LOGIN_USER_LOCKED.getResult();
            }

            //昵称太长 影响页面展示
            if (user.getNickName() != null && user.getNickName().length() > 7){
                String tmpUser = user.getNickName().substring(0,7)+"";
                user.setNickName(tmpUser);
            }

            NewBeeMallUserVO newBeeMallUserVO = new NewBeeMallUserVO();
            BeanUtil.copyProperties(user,newBeeMallUserVO);
            //设置购物车中的数量,? todo
            session.setAttribute(Constants.MALL_USER_SESSION_KEY,newBeeMallUserVO);
            return ServiceResultEnum.SUCCESS.getResult();
        }
        return ServiceResultEnum.LOGIN_ERROR.getResult();
    }
}
