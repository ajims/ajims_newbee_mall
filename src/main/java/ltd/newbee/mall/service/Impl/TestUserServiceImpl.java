package ltd.newbee.mall.service.Impl;

import ltd.newbee.mall.dao.TestUserDao;
import ltd.newbee.mall.entity.TestUser;
import ltd.newbee.mall.service.TestUserService;
import ltd.newbee.mall.util.PageResult;
import ltd.newbee.mall.util.PageQueryUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TestUserServiceImpl implements TestUserService {

    @Autowired
    private TestUserDao testUserDao;

    @Override
    public PageResult getAdminUserPage(PageQueryUtil pageQueryUtil) {
        //当前页码中的数据列表,page(第几页),limit(每页条数)
        List<TestUser> users = testUserDao.findUsers(pageQueryUtil);

        //数据总条数,用于计算分页数据
        int total = testUserDao.getTotalUser(pageQueryUtil);
        //总记录数,每页记录数,当前页数,列表数据
        PageResult pageResult = new PageResult(total, pageQueryUtil.getLimit(), pageQueryUtil.getPage(), users);

        return pageResult;
    }
}
