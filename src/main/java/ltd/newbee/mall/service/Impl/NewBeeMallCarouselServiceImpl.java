package ltd.newbee.mall.service.Impl;

import ltd.newbee.mall.dao.CarouselMapper;
import ltd.newbee.mall.entity.Carousel;
import ltd.newbee.mall.enums.ServiceResultEnum;
import ltd.newbee.mall.service.NewBeeMallCarouselService;
import ltd.newbee.mall.util.*;
import ltd.newbee.mall.vo.NewBeeMallIndexCarouselVO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
public class NewBeeMallCarouselServiceImpl implements NewBeeMallCarouselService {


    @Resource
    private CarouselMapper carouselMapper;

    @Override
    public PageResult getCarouselPage(PageQueryUtil pageQueryUtil) {
        List<Carousel> carousels = carouselMapper.findCarouselList(pageQueryUtil);
        int total = carouselMapper.getTotalCarousels(pageQueryUtil);
        PageResult pageResult = new PageResult(total, pageQueryUtil.getPage(), pageQueryUtil.getLimit(), carousels);
        return pageResult;
    }

    @Override
    public String saveCarousel(Carousel carousel) {
        if(carouselMapper.insertSelective(carousel) > 0){
            return ServiceResultEnum.SUCCESS.getResult();
        }

        return ServiceResultEnum.DB_ERROR.getResult();
    }

    @Override
    //更新Carousel
    public String updateCarousel(Carousel carousel) {
        //在更新前,是查找以id为线索查找,而不是更新
        Carousel tmp = carouselMapper.selectByPrimaryKey(carousel.getCarouselId());
        if(tmp == null){
            return ServiceResultEnum.DATA_NOT_EXIST.getResult();
        }

        tmp.setCarouselRank(carousel.getCarouselRank());
        tmp.setRedirectUrl(carousel.getRedirectUrl());
        tmp.setCarouselUrl(carousel.getCarouselUrl());
        tmp.setUpdateTime(new Date());

        if (carouselMapper.updateByPrimaryKeySelective(carousel) > 0){
            return ServiceResultEnum.SUCCESS.getResult();
        }

        return ServiceResultEnum.DB_ERROR.getResult();
    }

    //根据 id 获取单条轮播图记录接口
    public Carousel getCarouselById(Integer carouselId){
        return carouselMapper.selectByPrimaryKey(carouselId);
    }

    public Boolean deleteBatch(Integer[] ids){
        if(ids.length<1){
            ResultGenerator.genFailResult("参数异常");
        }
        //删除数据
        return carouselMapper.deleteBatch(ids) > 0;
    }

    @Override
    public List<NewBeeMallIndexCarouselVO> getCarouselsForIndex(int number) {
        List<NewBeeMallIndexCarouselVO> newBeeMallIndexCarouselVOS = new ArrayList<>(number);
        List<Carousel> carouselList = carouselMapper.findCarouselByNum(number);

        if(!CollectionUtils.isEmpty(carouselList)){
            newBeeMallIndexCarouselVOS = BeanUtil.copyList(carouselList, NewBeeMallIndexCarouselVO.class);
            //另一种写法   newBeeMallIndexCarouselVOS = BeanUtil.copyProperties(carouselList, newBeeMallIndexCarouselVOS);
        }
        return newBeeMallIndexCarouselVOS;
    }


}
