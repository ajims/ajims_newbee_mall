package ltd.newbee.mall.service.Impl;

import ltd.newbee.mall.dao.IndexConfigMapper;
import ltd.newbee.mall.dao.NewBeeMallGoodsMapper;
import ltd.newbee.mall.entity.IndexConfig;
import ltd.newbee.mall.entity.NewBeeMallGoods;
import ltd.newbee.mall.enums.ServiceResultEnum;
import ltd.newbee.mall.service.NewBeeMallIndexConfigService;
import ltd.newbee.mall.util.BeanUtil;
import ltd.newbee.mall.util.PageQueryUtil;
import ltd.newbee.mall.util.PageResult;
import ltd.newbee.mall.util.ResultGenerator;
import ltd.newbee.mall.vo.NewBeeMallIndexConfigGoodsVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class NewBeeMallIndexConfigServiceImpl implements NewBeeMallIndexConfigService {


    @Resource
    private IndexConfigMapper indexConfigMapper;

    @Resource
    private NewBeeMallGoodsMapper goodsMapper;

    @Override
    public PageResult getConfigsPage(PageQueryUtil pageQueryUtil) {
        //进行后台分页功能,常用的实现
        List<IndexConfig> indexConfigs = indexConfigMapper.findCarouselList(pageQueryUtil);
        //查询总共多少条配置的数据
        int total = indexConfigMapper.getTotalIndexConfigs(pageQueryUtil);

        //最重要的语句
        PageResult pageResult = new PageResult(total,pageQueryUtil.getPage(),pageQueryUtil.getLimit(),indexConfigs);
        return pageResult;
    }

    @Override
    public String saveIndexConfig(IndexConfig config) {
        if (indexConfigMapper.insertSelective(config) > 0){
            return ServiceResultEnum.SUCCESS.getResult();
        }
        return ServiceResultEnum.DB_ERROR.getResult();
    }

    @Override
    public String updateConfig(IndexConfig config) {
        //找有没有这个配置项
        IndexConfig tmp = indexConfigMapper.selectByPrimaryKey(config.getConfigId());
        if(tmp == null){
            return ServiceResultEnum.DB_ERROR.getResult();
        }

        if(indexConfigMapper.updateByPrimaryKeySelective(config) > 0){
            return ServiceResultEnum.SUCCESS.getResult();
        }
        return ServiceResultEnum.DB_ERROR.getResult();
    }

    @Override
    public Boolean deleteBatch(Long[] ids) {
        if (ids.length < 1){
            return false;
        }

        return indexConfigMapper.deleteBatch(ids) > 0;
    }

    @Override
    public IndexConfig getIndexConfigById(Long id) {
        return null;
    }

    @Override
    public List<NewBeeMallIndexConfigGoodsVO> getConfigGoodsesForIndex(int configType, int number) {

        List<NewBeeMallIndexConfigGoodsVO> newBeeMallIndexConfigGoodsVOS = new ArrayList<>(number);

        List<IndexConfig> indexConfigs = indexConfigMapper.findIndexConfigsByTypeAndNum(configType,number);

        if (!CollectionUtils.isEmpty(indexConfigs)){
            //取出所有的goodsId
            List<Long> goodsIds = indexConfigs.stream().map(IndexConfig::getGoodsId).collect(Collectors.toList());
            List<NewBeeMallGoods> newBeeMallGoods= goodsMapper.selectByPrimaryKeys(goodsIds);

            newBeeMallIndexConfigGoodsVOS = BeanUtil.copyList(newBeeMallGoods,NewBeeMallIndexConfigGoodsVO.class);

            for (NewBeeMallIndexConfigGoodsVO newBeeMallIndexConfigGoodsVO : newBeeMallIndexConfigGoodsVOS){
                String goodsName = newBeeMallIndexConfigGoodsVO.getGoodsName();
                String goodsIntro = newBeeMallIndexConfigGoodsVO.getGoodsIntro();

                //如果字符串过长导致文字超出的问题
                if(goodsName.length() > 30){
                    goodsName = goodsName.substring(0,30) + "....";
                    newBeeMallIndexConfigGoodsVO.setGoodsName(goodsName);
                }

                if(goodsIntro.length() > 30){
                    goodsIntro = goodsIntro.substring(0,30) + "....";
                    newBeeMallIndexConfigGoodsVO.setGoodsName(goodsIntro);
                }
            }
        }
        return newBeeMallIndexConfigGoodsVOS;
    }
}
