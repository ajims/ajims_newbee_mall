package ltd.newbee.mall.service;

import ltd.newbee.mall.util.PageQueryUtil;
import ltd.newbee.mall.util.PageResult;
import ltd.newbee.mall.vo.NewBeeMallOrderDetailVO;
import ltd.newbee.mall.vo.NewBeeMallShoppingCartItemVO;
import ltd.newbee.mall.vo.NewBeeMallUserVO;

import java.util.List;

public interface newBeeMallOrderService {

    String saveOrder(NewBeeMallUserVO user, List<NewBeeMallShoppingCartItemVO> myShoppingCartItems);

    NewBeeMallOrderDetailVO getOrderDetailByOrderNo(String orderNo, Long userId);

    /**
     * 我的订单列表
     *
     * @param pageQueryUtil
     * @return
     */
    PageResult getMyOrders(PageQueryUtil pageQueryUtil);

    String paySuccess(String orderNo, int payType);
}
