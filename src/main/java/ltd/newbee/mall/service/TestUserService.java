package ltd.newbee.mall.service;

import ltd.newbee.mall.util.PageResult;
import ltd.newbee.mall.util.PageQueryUtil;

public interface TestUserService {

    /*
    * 分页功能
    *
    * */
    PageResult getAdminUserPage(PageQueryUtil pageQueryUtil);
}
