package ltd.newbee.mall.service;

import ltd.newbee.mall.entity.newbeeMallShoppingCartItem;
import ltd.newbee.mall.vo.NewBeeMallShoppingCartItemVO;

import java.util.List;

public interface newBeeMallShoppingCartService {

    //保存商品至购物车
    String saveNewBeeMallCartItem(newbeeMallShoppingCartItem newbeeMallShoppingCartItem);

    //修改购物车中的属性
    String updateNewBeeMallCartItem(newbeeMallShoppingCartItem newBeeMallShoppingCartItem);

    List<NewBeeMallShoppingCartItemVO> getMyShoppingCartItems(Long newBeeMallUserId);

    Boolean deleteById(Long newBeeMallShoppingCartItemId);
}
