package ltd.newbee.mall.dao;

import ltd.newbee.mall.entity.TestUser;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;
@Mapper
public interface TestUserDao {

    //这个不是只查询出一个人的信息,而是所有人的所有信息,根据参数查询用户列表
    List<TestUser> findUsers(Map param);

    /**
     * 查询用户总数
     *
     * @param param
     * @return
     */
    int getTotalUser(Map param);
}
