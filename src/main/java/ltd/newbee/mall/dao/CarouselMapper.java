package ltd.newbee.mall.dao;

import ltd.newbee.mall.entity.Carousel;
import ltd.newbee.mall.util.PageQueryUtil;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface CarouselMapper {

    List<Carousel> findCarouselList(PageQueryUtil pageQueryUtil);

    int getTotalCarousels(PageQueryUtil pageQueryUtil);

    //返回的是被影响的数据的行数
    int insertSelective(Carousel carousel);

    Carousel updateByPrimaryKey(Integer carouselId);


    int updateByPrimaryKeySelective(Carousel record);

    //批量删除轮播图内容 todo ?为什么是int
    int deleteBatch(Integer[] ids);

    Carousel selectByPrimaryKey(Integer carouselId);

    List<Carousel> findCarouselByNum(@Param("number") int number);
}
