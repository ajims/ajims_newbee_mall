package ltd.newbee.mall.dao;

import ltd.newbee.mall.entity.IndexConfig;
import ltd.newbee.mall.entity.NewBeeMallGoods;
import ltd.newbee.mall.util.PageQueryUtil;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface IndexConfigMapper {

    //为了分页查询的数据库语句
    List<IndexConfig> findCarouselList(PageQueryUtil pageQueryUtil);

    int getTotalIndexConfigs(PageQueryUtil pageQueryUtil);

    //对数据库添加配置的新的数据
    int insertSelective(IndexConfig indexConfig);


    //通过相关id查询config
    IndexConfig selectByPrimaryKey(Long configId);

    int updateByPrimaryKeySelective(IndexConfig record);

    //删除
    int deleteBatch(Long[] ids);

    List<IndexConfig> findIndexConfigsByTypeAndNum(int configType, int number);

}
