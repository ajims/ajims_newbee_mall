package ltd.newbee.mall.dao;

import ltd.newbee.mall.entity.GoodsCategory;
import ltd.newbee.mall.util.PageQueryUtil;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface GoodsCategoryMapper {

    List<GoodsCategory> findGoodsCategoryList(PageQueryUtil pageQueryUtil);

    int getTotalCategory(PageQueryUtil pageQueryUtil);

    GoodsCategory selectByLevelAndName(@Param("categoryLevel") Byte CategoryLevel, @Param("categoryName") String CategoryName);

    int insertSelective(GoodsCategory goodsCategory);

    GoodsCategory selectByPrimaryKey(Long categoryId);

    int updateByPrimaryKeySelective(GoodsCategory record);

    int deleteBatch(Integer[] ids);

//    List<GoodsCategory> selectByLevelAndParentIdsAndNumber(@Param("parentIds") List<Long> parentIds,
//                                                           @Param("categoryLevel") int categoryLevel, @Param("number") int number);


    List<GoodsCategory> selectByLevelAndParentIdsAndNumber(@Param("parentIds") List<Long> parentIds,
                                                           @Param("categoryLevel") int categoryLevel, @Param("number") int number);


}
