package ltd.newbee.mall.dao;

import ltd.newbee.mall.DTO.StockNumDTO;
import ltd.newbee.mall.entity.NewBeeMallGoods;
import ltd.newbee.mall.util.PageQueryUtil;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface NewBeeMallGoodsMapper {
    int insertSelective(NewBeeMallGoods record);

    NewBeeMallGoods selectByPrimaryKey(Long goodsId);

    int updateByPrimaryKeySelective(NewBeeMallGoods newBeeMallGoods);

    List<NewBeeMallGoods> findNewBeeMallGoodsList(PageQueryUtil pageQueryUtil);

    int getTotalNewBeeMallGoods(PageQueryUtil pageQueryUtil);

    int batchUpdateSellStatus(@Param("goodsId") Long[] ids,@Param("sellStatus") int sellStatus);

    List<NewBeeMallGoods> selectByPrimaryKeys(List<Long> ids);

    List<NewBeeMallGoods> findNewBeeMallGoodsListBySearch(PageQueryUtil pageQueryUtil);

    int getTotalNewBeeMallGoodsBySearch(PageQueryUtil pageQueryUtil);

    //todo 标记
    int updateStockNum(@Param("stockNumDTOS") List<StockNumDTO> stockNumDTOS);

}
