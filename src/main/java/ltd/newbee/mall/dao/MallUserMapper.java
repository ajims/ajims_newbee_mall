package ltd.newbee.mall.dao;

import ltd.newbee.mall.entity.MallUser;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface MallUserMapper {

    MallUser selectByLoginName(String loginName);

    int insertSelective(MallUser register);

    MallUser selectByLoginNameAndPasswd(@Param("loginName") String loginName, @Param("password") String password);
}
