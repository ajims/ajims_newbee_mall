package ltd.newbee.mall.dao;

import ltd.newbee.mall.entity.AdminUser;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.web.bind.annotation.RequestParam;
@Mapper
public interface AdminUserMapper {

    //登录方法
    AdminUser login(@RequestParam("userName") String userName,@RequestParam("password") String password);

    AdminUser selectByPrimaryKey(Integer adminUserId);

    //根据逐渐查询用户并修改其账号或密码
    int updateByPrimaryKeySelective(AdminUser record);



}
