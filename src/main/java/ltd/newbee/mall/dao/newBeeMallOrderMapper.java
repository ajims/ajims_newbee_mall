package ltd.newbee.mall.dao;

import ltd.newbee.mall.entity.NewBeeMallOrder;
import ltd.newbee.mall.entity.NewBeeMallOrderItem;
import ltd.newbee.mall.util.PageQueryUtil;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface newBeeMallOrderMapper {

    int insertSelective(NewBeeMallOrder order);

    NewBeeMallOrder selectByOrderNo(String orderNo);

    int getTotalNewBeeMallOrders(PageQueryUtil pageQueryUtil);

    List<NewBeeMallOrder> findNewBeeMallOrderList(PageQueryUtil pageQueryUtil);

    int updateByPrimaryKeySelective(NewBeeMallOrder record);

}
