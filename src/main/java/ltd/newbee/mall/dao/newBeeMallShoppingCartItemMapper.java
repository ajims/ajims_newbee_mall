package ltd.newbee.mall.dao;

import ltd.newbee.mall.entity.newbeeMallShoppingCartItem;
import ltd.newbee.mall.vo.NewBeeMallShoppingCartItemVO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface newBeeMallShoppingCartItemMapper {

    //这里的Param里的值就是在.xml里where处赋值的参数
    newbeeMallShoppingCartItem selectByUserIdAndGoodsId(@Param("newBeeMallUserId") Long newBeeMallUserId, @Param("goodsId") Long goodsId);

    newbeeMallShoppingCartItem selectByPrimaryKey(Long cartItemId);

    int updateByPrimaryKeySelective(newbeeMallShoppingCartItem record);

    int selectCountByUserId(Long newBeeMallUserId);

    int insertSelective(newbeeMallShoppingCartItem record);

    List<newbeeMallShoppingCartItem> selectByUserId(@Param("newBeeMallUserId") Long newBeeMallUserId, @Param("number") int number);

    int deleteByPrimaryKey(Long cartItemId);

    int deleteBatch(List<Long> id);
}
