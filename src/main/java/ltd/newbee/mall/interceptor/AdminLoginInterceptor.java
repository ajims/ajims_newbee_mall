package ltd.newbee.mall.interceptor;

import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 后台系统身份验证拦截器
 * preHandle：在业务处理器处理请求之前被调用。预处理，可以进行编码、安全控制、权限校验等处理;
 * postHandle：在业务处理器处理请求执行完成后，生成视图之前执行。
 * afterCompletion：在DispatcherServlet完全处理完请求后被调用，可用于清理资源等，返回处理（已经渲染了页面）;
 */
@Component
public class AdminLoginInterceptor implements HandlerInterceptor {

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {

        System.out.println("进入拦截器");
        String  uri = request.getRequestURI();
        if (uri.startsWith("/admin")&& null==request.getSession().getAttribute("loginUser")){
            //这个就是弹窗信息或者是提示信息
            request.getSession().setAttribute("errorMsg", "请登录");
            response.sendRedirect(request.getContextPath()+"/admin/login");
            System.out.println("未登录,拦截成功");
            return false;
        }else {
            request.getSession().removeAttribute("errorMsg");
            System.out.println("已登录,放行");
            return true;
        }
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {

    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {

    }
}
