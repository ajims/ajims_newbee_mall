package ltd.newbee.mall.entity;

import lombok.Data;

import java.io.Serializable;

@Data
public class TestUser implements Serializable {
    private Long id;

    private String UserName;

    private String password;
}
