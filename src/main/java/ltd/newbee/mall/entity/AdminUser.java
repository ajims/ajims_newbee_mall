package ltd.newbee.mall.entity;

import lombok.Data;
//后台登录用
@Data
public class AdminUser {

    private int adminUserId;

    private String loginUserName;

    private String loginPassword;

    private String nickName;

    private byte locked;
}
