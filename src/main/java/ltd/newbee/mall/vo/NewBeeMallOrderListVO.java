package ltd.newbee.mall.vo;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * 订单列表页面VO
 */
@Data
public class NewBeeMallOrderListVO implements Serializable {

    private Long orderId;

    //订单号
    private String orderNo;
    //订单总价
    private Integer totalPrice;
    //支付方式
    private Byte payType;
    //订单状态
    private Byte orderStatus;
    //订单状态对应的文案
    private String orderStatusString;

    private String userAddress;

    //交易时间
    private Date createTime;
    //订单项列表
    private List<NewBeeMallOrderItemVO> newBeeMallOrderItemVOS;
}
