package ltd.newbee.mall.vo;

import lombok.Data;

import java.io.Serializable;
/**
 * 首页轮播图VO
 */
@Data
public class NewBeeMallIndexCarouselVO implements Serializable {
    private String carouselUrl;

    private String redirectUrl;
}
