package ltd.newbee.mall.vo;

import lombok.Data;

import java.io.Serializable;

/*
* 购物项列表数据和底部的小计数据，列表数据中包括商品标题字段、商品预览图字段、商品价格字段，这些字段我们可以通过购物项表中的 goods_id 来查询，
* 还有商品数量字段、单商品总价字段，列表最后还有一个删除按钮，这就表示我们还需要把购物项 id 字段也返回给前端。
* */
@Data
public class NewBeeMallShoppingCartItemVO implements Serializable {

    /*
     * 购物车页面购物项VO
     */
    private Long cartItemId;

    private Long goodsId;

    private String goodsName;

    private Integer goodsCount;

    private String goodsCoverImg;

    private Integer sellingPrice;
}
