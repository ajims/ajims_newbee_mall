package ltd.newbee.mall.controller.mall;

import ltd.newbee.mall.common.Constants;
import ltd.newbee.mall.entity.NewBeeMallGoods;
import ltd.newbee.mall.service.NewBeeMallCategoryService;
import ltd.newbee.mall.service.newBeeMallGoodsService;
import ltd.newbee.mall.util.BeanUtil;
import ltd.newbee.mall.util.PageQueryUtil;
import ltd.newbee.mall.util.Result;
import ltd.newbee.mall.vo.NewBeeMallGoodsDetailVO;
import ltd.newbee.mall.vo.SearchPageCategoryVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.thymeleaf.util.StringUtils;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.Map;
//34
@Controller
public class GoodsController {

    @Resource
    private NewBeeMallCategoryService newBeeMallCategoryService;

    @Resource
    private newBeeMallGoodsService newBeeMallGoodsService;


    /*??todo p33*/
    @GetMapping({"/search", "/search.html"})
    public String searchPage(@RequestParam Map<String, Object> param, HttpServletRequest request) {
        if (StringUtils.isEmpty((String) param.get("page"))) {
            param.put("page", 1);
        }

        param.put("limit", Constants.GOODS_SEARCH_PAGE_LIMIT);

        //封装分类数据
        if (param.containsKey("goodsCategoryId") && !StringUtils.isEmpty(param.get("goodsCategoryId") + "")) {
            Long categoryId = Long.valueOf(param.get("goodsCategoryId") + "");

            SearchPageCategoryVO searchPageCategoryVO = newBeeMallCategoryService.getCategoriesForSearch(categoryId);

            if (searchPageCategoryVO != null) {
                request.setAttribute("goodsCategoryId", categoryId);
                request.setAttribute("searchPageCategoryVO", searchPageCategoryVO);
            }
        }

        //封装参数供前端回显
        if (param.containsKey("orderBy") && !StringUtils.isEmpty(param.get("orderBy") + "")) {
            request.setAttribute("orderBy", param.get("orderBy") + "");
        }

        String keyword = "";
        if (param.containsKey("keyword") && !StringUtils.isEmpty(param.get("keyword") + "")) {
            keyword = param.get("keyword") + "";
        }
            //封装商品数据
            request.setAttribute("keyword",keyword);
            param.put("keyword",keyword);

            //封装商品数据
            PageQueryUtil pageQueryUtil = new PageQueryUtil(param);
            request.setAttribute("pageResult",newBeeMallGoodsService.searchNewBeeMallGoods(pageQueryUtil));
            return "mall/search";

    }

    /*商品详情页*/
    @GetMapping("/goods/detail/{goodsId}")
    public String detailPage(@PathVariable("goodsId") Long goodsId,HttpServletRequest request){
        if (goodsId < 1){
            return "error/error_5xx";
        }
        NewBeeMallGoods goods = newBeeMallGoodsService.getNewBeeMallGoodsById(goodsId);
        if (goods == null){
            return "error/error_404";
        }

        NewBeeMallGoodsDetailVO newBeeMallGoodsDetailVO = new NewBeeMallGoodsDetailVO();
        BeanUtil.copyProperties(goods,newBeeMallGoodsDetailVO);
        request.setAttribute("goodsDetail",newBeeMallGoodsDetailVO);
        return "mall/detail";
    }


}
