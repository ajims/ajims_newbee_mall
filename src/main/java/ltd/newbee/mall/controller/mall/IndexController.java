package ltd.newbee.mall.controller.mall;

import ltd.newbee.mall.common.Constants;
import ltd.newbee.mall.enums.IndexConfigTypeEnum;
import ltd.newbee.mall.service.NewBeeMallCarouselService;
import ltd.newbee.mall.service.NewBeeMallCategoryService;
import ltd.newbee.mall.service.NewBeeMallIndexConfigService;
import ltd.newbee.mall.vo.NewBeeMallIndexCarouselVO;
import ltd.newbee.mall.vo.NewBeeMallIndexCategoryVO;
import ltd.newbee.mall.vo.NewBeeMallIndexConfigGoodsVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Controller
public class IndexController {

    @Autowired
    private NewBeeMallCarouselService newBeeMallCarouselService;

    @Autowired
    private NewBeeMallCategoryService newBeeMallCategoryService;

    @Resource
    private NewBeeMallIndexConfigService newBeeMallIndexConfigService;

    @GetMapping({"/index", "/", "/index.html"})
    public String indexPage(HttpServletRequest request) {

        /*
        * 作用:我们在后台管理系统中已经配置了多个轮播图数据，所以需要通过后台将这些数据读取出来并放到 request 中，之后在页面中使用 th 语法将这些数据渲染到页面上。
        * */
        List<NewBeeMallIndexCarouselVO> carousels = newBeeMallCarouselService.getCarouselsForIndex(Constants.INDEX_CAROUSEL_NUMBER);

        List<NewBeeMallIndexCategoryVO> categories = newBeeMallCategoryService.getCategoriesForIndex();

        List<NewBeeMallIndexConfigGoodsVO> hotGoodses = newBeeMallIndexConfigService
                .getConfigGoodsesForIndex(IndexConfigTypeEnum.INDEX_GOODS_HOT.getType(),Constants.INDEX_GOODS_HOT_NUMBER);

        List<NewBeeMallIndexConfigGoodsVO> newGoodses = newBeeMallIndexConfigService
                .getConfigGoodsesForIndex(IndexConfigTypeEnum.INDEX_GOODS_NEW.getType(),Constants.INDEX_GOODS_NEW_NUMBER);

        List<NewBeeMallIndexConfigGoodsVO> recommendGoodses = newBeeMallIndexConfigService
                .getConfigGoodsesForIndex(IndexConfigTypeEnum.INDEX_GOODS_RECOMMOND.getType(),Constants.INDEX_GOODS_RECOMMOND_NUMBER);

        request.setAttribute("carousels", carousels);//轮播图

        request.setAttribute("categories", categories);//分类数据

        request.setAttribute("hotGoodses",hotGoodses);  //热销商品

        request.setAttribute("newGoodses",newGoodses);  //新品

        request.setAttribute("recommendGoodses",recommendGoodses); //推荐商品

        return "/mall/index";
    }
}
