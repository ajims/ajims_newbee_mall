package ltd.newbee.mall.controller.mall;

import ltd.newbee.mall.common.Constants;
import ltd.newbee.mall.entity.MallUser;
import ltd.newbee.mall.entity.newbeeMallShoppingCartItem;
import ltd.newbee.mall.enums.ServiceResultEnum;
import ltd.newbee.mall.service.newBeeMallShoppingCartService;
import ltd.newbee.mall.util.Result;
import ltd.newbee.mall.util.ResultGenerator;
import ltd.newbee.mall.vo.NewBeeMallGoodsDetailVO;
import ltd.newbee.mall.vo.NewBeeMallShoppingCartItemVO;
import ltd.newbee.mall.vo.NewBeeMallUserVO;
import org.springframework.stereotype.Controller;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

@Controller
public class ShoppingCartController {

    @Resource
    private newBeeMallShoppingCartService newBeeMallShoppingCartService;

    //?
    //将商品放入购物车,商品是一个整体
    @PostMapping("/shop-cart")
    @ResponseBody
    public Result saveNewBeeMallShoppingCartItem(@RequestBody newbeeMallShoppingCartItem newbeeMallShoppingCartItem,
                                                 HttpSession session) {
        NewBeeMallUserVO user = (NewBeeMallUserVO) session.getAttribute(Constants.MALL_USER_SESSION_KEY);

        //得到用户的id,去查找数据库
        newbeeMallShoppingCartItem.setUserId(user.getUserId());
        String saveResult = newBeeMallShoppingCartService.saveNewBeeMallCartItem(newbeeMallShoppingCartItem);

        //添加购物车成功
        if (ServiceResultEnum.SUCCESS.getResult().equals(saveResult)) {
            return ResultGenerator.genSuccessResult();
        }

        //添加购物车失败
        return ResultGenerator.genFailResult(saveResult);
    }

    @GetMapping("/shop-cart")
    public String cartListPage(HttpServletRequest request,
                               HttpSession session) {
        //NewBeeMallUserVO user1 = (NewBeeMallUserVO) request.getAttribute(Constants.MALL_USER_SESSION_KEY);
        NewBeeMallUserVO user = (NewBeeMallUserVO) session.getAttribute(Constants.MALL_USER_SESSION_KEY);

        int itemTotal = 0;
        int priceTotal = 0;

        List<NewBeeMallShoppingCartItemVO> myShoppingCartItems = newBeeMallShoppingCartService.getMyShoppingCartItems(user.getUserId());
        if (!CollectionUtils.isEmpty(myShoppingCartItems)) {
            //购物项总数
            //stream.mapToInt是获得NewBeeMallShoppingCartItemVO::getGoodsCount的数值
            itemTotal = myShoppingCartItems.stream().mapToInt(NewBeeMallShoppingCartItemVO::getGoodsCount).sum();
            if (itemTotal < 1) {
                return "error/error_5xx";
            }

            //NewBeeMallShoppingCartItemVO代表一件商品的所有信息
            for (NewBeeMallShoppingCartItemVO NewBeeMallShoppingCartItemVO : myShoppingCartItems) {
                priceTotal += NewBeeMallShoppingCartItemVO.getGoodsCount() * NewBeeMallShoppingCartItemVO.getSellingPrice();
            }

            if (priceTotal < 1){
                return "error/error_5xx";
            }
        }

        // 注意
        request.setAttribute("itemsTotal",itemTotal);
        request.setAttribute("priceTotal",priceTotal);
        request.setAttribute("myShoppingCartItems",myShoppingCartItems);
        return "mall/cart";
    }


    //修改购物车详情页的商品数量
    //@RequestBody通常是一个VO类作为传递的主体
    @PutMapping("/shop-cart")
    @ResponseBody
    public Result updateNewBeeMallShoppingCartItem(@RequestBody newbeeMallShoppingCartItem newBeeMallShoppingCartItem,
                                                   HttpSession httpSession) {
        NewBeeMallUserVO user = (NewBeeMallUserVO) httpSession.getAttribute(Constants.MALL_USER_SESSION_KEY);
        newBeeMallShoppingCartItem.setUserId(user.getUserId());
        String updateResult = newBeeMallShoppingCartService.updateNewBeeMallCartItem(newBeeMallShoppingCartItem);
        //修改成功
        if (ServiceResultEnum.SUCCESS.getResult().equals(updateResult)) {
            return ResultGenerator.genSuccessResult();
        }
        //修改失败
        return ResultGenerator.genFailResult(updateResult);
    }

    //删除功能
    @DeleteMapping("/shop-cart/{newBeeMallShoppingCartItemId}")
    @ResponseBody
    public Result updateNewBeeMallShoppingCartItem(@PathVariable("newBeeMallShoppingCartItemId") Long newBeeMallShoppingCartItemId,
                                                   HttpSession session){
        NewBeeMallUserVO user = (NewBeeMallUserVO) session.getAttribute(Constants.MALL_USER_SESSION_KEY);

        Boolean deleteResult = newBeeMallShoppingCartService.deleteById(newBeeMallShoppingCartItemId);

        //删除成功
        if (deleteResult) {
            return ResultGenerator.genSuccessResult();
        }
        //删除失败
        return ResultGenerator.genFailResult(ServiceResultEnum.OPERATE_ERROR.getResult());
    }

    //lesson 36
    @GetMapping("/shop-cart/settle")
    public String settlePage(HttpServletRequest request,
                             HttpSession session){
        int totalPrice = 0;

        //session是从已登录的账号里获取用户信息
        NewBeeMallUserVO user = (NewBeeMallUserVO) session.getAttribute(Constants.MALL_USER_SESSION_KEY);

        List<NewBeeMallShoppingCartItemVO> myShoppingCartItems = newBeeMallShoppingCartService.getMyShoppingCartItems(user.getUserId());

        if (CollectionUtils.isEmpty(myShoppingCartItems)){
            return "/shop-cart";
        }else{
            for (NewBeeMallShoppingCartItemVO newBeeMallShoppingCartItemVO : myShoppingCartItems){
                totalPrice += newBeeMallShoppingCartItemVO.getGoodsCount() * newBeeMallShoppingCartItemVO.getSellingPrice();
            }

            if (totalPrice < 1) {
                return "error/error_5xx";
            }

            request.setAttribute("priceTotal",totalPrice);
            request.setAttribute("myShoppingCartItems",myShoppingCartItems);
            return "mall/order-settle";
        }
    }
}
