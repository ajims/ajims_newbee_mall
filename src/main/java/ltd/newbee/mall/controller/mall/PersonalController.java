package ltd.newbee.mall.controller.mall;

import ltd.newbee.mall.common.Constants;
import ltd.newbee.mall.enums.ServiceResultEnum;
import ltd.newbee.mall.service.NewBeeMallUserService;
import ltd.newbee.mall.util.MD5Util;
import ltd.newbee.mall.util.Result;
import ltd.newbee.mall.util.ResultGenerator;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.thymeleaf.util.StringUtils;

import javax.annotation.Resource;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@Controller
public class PersonalController {

    @Resource
    private NewBeeMallUserService newBeeMallUserService;

    /*
    * 登录界面跳转
    * */
    @GetMapping({"/login","login.html"})
    public String loginPage(){
        return "mall/login";
    }

    /*
    * 注册界面跳转
    * */
    @GetMapping({"register","register.html"})
    public String registerPage(){
        return "mall/register";
    }

    /*
    * 登录
    * */
    @PostMapping("/login")
    @ResponseBody
    public Result login(@RequestParam("loginName") String loginName,
                        @RequestParam("verifyCode") String verifyCode,
                        @RequestParam("password") String password,
                        HttpSession session){
        if (StringUtils.isEmpty(loginName)){
            ResultGenerator.genFailResult(ServiceResultEnum.LOGIN_NAME_NULL.getResult());
        }

        if (StringUtils.isEmpty(verifyCode)){
            ResultGenerator.genFailResult(ServiceResultEnum.LOGIN_PASSWORD_NULL.getResult());
        }

        if (StringUtils.isEmpty(password)){
            ResultGenerator.genFailResult(ServiceResultEnum.LOGIN_PASSWORD_NULL.getResult());
        }

        //验证码
        String katchaCode = session.getAttribute(Constants.MALL_VERIFY_CODE_KEY) + "";

        if (StringUtils.isEmpty(katchaCode) || !verifyCode.equals(katchaCode)){
            return ResultGenerator.genFailResult(ServiceResultEnum.LOGIN_VERIFY_CODE_ERROR.getResult());
        }

        String loginResult =newBeeMallUserService.login(loginName, MD5Util.MD5Encode(password,"UTF-8"),session);

        //登录成功
        if(ServiceResultEnum.SUCCESS.getResult().equals(loginResult)){
            return ResultGenerator.genSuccessResult();
        }

        //登录失败
        return ResultGenerator.genFailResult("登录失败");
    }


    /*
    * 注册
    * */
    @PostMapping("/register")
    @ResponseBody
    public Result register(@RequestParam("loginName") String loginName,
                           @RequestParam("verifyCode") String verifyCode,
                           @RequestParam("password") String password,
                           HttpSession session){

        if (StringUtils.isEmpty(loginName)){
            ResultGenerator.genFailResult(ServiceResultEnum.LOGIN_NAME_NULL.getResult());
        }

        if (StringUtils.isEmpty(verifyCode)){
            ResultGenerator.genFailResult(ServiceResultEnum.LOGIN_PASSWORD_NULL.getResult());
        }

        if (StringUtils.isEmpty(password)){
            ResultGenerator.genFailResult(ServiceResultEnum.LOGIN_PASSWORD_NULL.getResult());
        }

        //验证码
        String katchaCode = session.getAttribute(Constants.MALL_VERIFY_CODE_KEY) + "";

        if (StringUtils.isEmpty(katchaCode) || !verifyCode.equals(katchaCode)){
            return ResultGenerator.genFailResult(ServiceResultEnum.LOGIN_VERIFY_CODE_ERROR.getResult());
        }

        String registerResult = newBeeMallUserService.register(loginName,password);

        //注册成功
        if(ServiceResultEnum.SUCCESS.getResult().equals(registerResult)){
            return ResultGenerator.genSuccessResult();
        }

        //注册失败
        return ResultGenerator.genFailResult("注册失败");
    }

    /*
    * 登出
    * */
    @GetMapping("/logout")
    public String logout(HttpSession session){
        session.removeAttribute(Constants.MALL_USER_SESSION_KEY);
        return "mall/login";
    }
}
