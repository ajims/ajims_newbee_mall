package ltd.newbee.mall.controller;

import ltd.newbee.mall.service.TransactionTestService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;

@Controller
public class TransactionTestController {
    @Resource
    private TransactionTestService transactionTestService;

    @GetMapping("/transactionTest")
    @ResponseBody
    public String transactionTest(){
        //事务管理测试
        // test1 未添加 @Transactional 注解
        transactionTestService.test1();
        // test2 添加了 @Transactional 注解
        transactionTestService.test2();

        return "请求失败";
    }
}
