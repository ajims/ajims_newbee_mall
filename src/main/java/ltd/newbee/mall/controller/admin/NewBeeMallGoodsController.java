package ltd.newbee.mall.controller.admin;

import ltd.newbee.mall.common.Constants;
import ltd.newbee.mall.entity.GoodsCategory;
import ltd.newbee.mall.entity.NewBeeMallGoods;
import ltd.newbee.mall.enums.NewBeeMallCategoryLevelEnum;
import ltd.newbee.mall.enums.ServiceResultEnum;
import ltd.newbee.mall.service.NewBeeMallCategoryService;
import ltd.newbee.mall.service.newBeeMallGoodsService;
import ltd.newbee.mall.util.PageQueryUtil;
import ltd.newbee.mall.util.Result;
import ltd.newbee.mall.util.ResultGenerator;
import org.springframework.stereotype.Controller;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;

@Controller
@RequestMapping("/admin")
public class NewBeeMallGoodsController {

    @Resource
    private NewBeeMallCategoryService newBeeMallCategoryService;

    @Resource
    private newBeeMallGoodsService newBeeMallGoodsService;

    //todo ???商品信息编辑页面制作
    @GetMapping("/goods/edit")
    public String edit(HttpServletRequest request){
        request.setAttribute("path", "edit");
        //查询所有一级分类
        List<GoodsCategory> categories = newBeeMallCategoryService.
                selectByLevelAndParentIdsAndNumber(Collections.singletonList(0L),
                        NewBeeMallCategoryLevelEnum.LEVEL_ONE.getLevel());

        //查询一级分类列表中第一个实体的所有二级分类
        if(!CollectionUtils.isEmpty(categories)){
            List<GoodsCategory> secondLevelCategories = newBeeMallCategoryService.
                    selectByLevelAndParentIdsAndNumber(Collections.singletonList(categories.get(0).getCategoryId()),
                            NewBeeMallCategoryLevelEnum.LEVEL_TWO.getLevel());
            if (!CollectionUtils.isEmpty(secondLevelCategories)){
                List<GoodsCategory> thirdLevelCategories = newBeeMallCategoryService.
                        selectByLevelAndParentIdsAndNumber(Collections.singletonList(secondLevelCategories.get(0).getCategoryId()),
                                NewBeeMallCategoryLevelEnum.LEVEL_THREE.getLevel());
                request.setAttribute("firstLevelCategories", categories);
                request.setAttribute("secondLevelCategories", secondLevelCategories);
                request.setAttribute("thirdLevelCategories", thirdLevelCategories);

                return "admin/newbee_mall_goods_edit";
            }
        }

        return "admin/index";
    }

    @PostMapping("/goods/save")
    @ResponseBody
    public Result save(@RequestBody NewBeeMallGoods newBeeMallGoods){
        if (StringUtils.isEmpty(newBeeMallGoods.getGoodsName())
                || StringUtils.isEmpty(newBeeMallGoods.getGoodsIntro())
                || StringUtils.isEmpty(newBeeMallGoods.getTag())
                || Objects.isNull(newBeeMallGoods.getOriginalPrice())
                || Objects.isNull(newBeeMallGoods.getGoodsCategoryId())
                || Objects.isNull(newBeeMallGoods.getSellingPrice())
                || Objects.isNull(newBeeMallGoods.getStockNum())
                || Objects.isNull(newBeeMallGoods.getGoodsSellStatus())
                || StringUtils.isEmpty(newBeeMallGoods.getGoodsCoverImg())
                || StringUtils.isEmpty(newBeeMallGoods.getGoodsDetailContent())) {
            return ResultGenerator.genFailResult("参数异常！");
        }

        String result = newBeeMallGoodsService.saveNewBeeMallGoods(newBeeMallGoods);
        if(ServiceResultEnum.SUCCESS.getResult().equals(result)){
            return ResultGenerator.genSuccessResult();
        }else {
            return ResultGenerator.genFailResult(result);
        }
    }


    @GetMapping("/goods/edit/{goodsId}")
    public String edit(HttpServletRequest request,@PathVariable("goodsId")Long goodsId){
        request.setAttribute("path", "edit");

        //todo ?   1.通过Id查询出相应的NewBeeMallGoods
        NewBeeMallGoods newBeeMallGoods = newBeeMallGoodsService.getNewBeeMallGoodsById(goodsId);
        if (newBeeMallGoods == null){
            //todo 暂时的失败返回地址
            return "admin/index";
        }

        if(newBeeMallGoods.getGoodsCategoryId() > 0 || newBeeMallGoods.getGoodsCategoryId() != null || newBeeMallGoods.getGoodsCategoryId() < 4){
            //查询当前商品所在目录名称
            GoodsCategory currentGoodsCategory = newBeeMallCategoryService.getCategoryById(newBeeMallGoods.getGoodsCategoryId());

            //todo == .equal()???
            if(currentGoodsCategory != null && currentGoodsCategory.getCategoryLevel() == NewBeeMallCategoryLevelEnum.LEVEL_THREE.getLevel()){
                //获取所有的一级分类
                List<GoodsCategory> firstLevelCategories = newBeeMallCategoryService.
                        selectByLevelAndParentIdsAndNumber(Collections.singletonList(0L), NewBeeMallCategoryLevelEnum.LEVEL_ONE.getLevel());

                //根据parentId查询当前parentId下所有的三级分类
                List<GoodsCategory> thirdLevelCategories = newBeeMallCategoryService.
                        selectByLevelAndParentIdsAndNumber(Collections.singletonList(currentGoodsCategory.getParentId()),
                                NewBeeMallCategoryLevelEnum.LEVEL_THREE.getLevel());

                //查询当前三级分类的父级二级分类
                GoodsCategory secondCategory = newBeeMallCategoryService.getCategoryById(currentGoodsCategory.getParentId());
                if(secondCategory != null){
                    //根据parentId查询当前parentId下所有的二级分类
                    List<GoodsCategory> secondLevelCategories = newBeeMallCategoryService.
                            selectByLevelAndParentIdsAndNumber(Collections.singletonList(currentGoodsCategory.getParentId()),
                                    NewBeeMallCategoryLevelEnum.LEVEL_THREE.getLevel());
                    //查询当前二级分类的父类一级分类
                    GoodsCategory firstCategory = newBeeMallCategoryService.
                            getCategoryById(secondCategory.getParentId());

                    if(firstCategory != null){
                        request.setAttribute("firstLevelCategories", firstLevelCategories);
                        request.setAttribute("secondLevelCategories", secondLevelCategories);
                        request.setAttribute("thirdLevelCategories", thirdLevelCategories);
                        request.setAttribute("firstLevelCategoryId", firstCategory.getCategoryId());
                        request.setAttribute("secondCategoryId", secondCategory.getCategoryId());
                        request.setAttribute("thirdLevelCategoryId", currentGoodsCategory.getCategoryId());
                    }
                }
            }
        }


        if(newBeeMallGoods.getGoodsCategoryId() == 0){
            //查询所有一级分类
            List<GoodsCategory> firstLevelCategories = newBeeMallCategoryService.
                    selectByLevelAndParentIdsAndNumber(Collections.singletonList(0L),
                            NewBeeMallCategoryLevelEnum.LEVEL_ONE.getLevel());
            if (!CollectionUtils.isEmpty(firstLevelCategories)){
                //查询所有二级分类
                List<GoodsCategory> secondLevelCategories = newBeeMallCategoryService.
                        selectByLevelAndParentIdsAndNumber(Collections.singletonList(firstLevelCategories.get(0).getCategoryId()),
                                NewBeeMallCategoryLevelEnum.LEVEL_TWO.getLevel());
                if (!CollectionUtils.isEmpty(secondLevelCategories)) {
                    //查询二级分类列表中第一个实体的所有三级分类
                    List<GoodsCategory> thirdLevelCategories = newBeeMallCategoryService.
                            selectByLevelAndParentIdsAndNumber(Collections.singletonList(secondLevelCategories.get(0).getCategoryId()),
                                    NewBeeMallCategoryLevelEnum.LEVEL_THREE.getLevel());
                    request.setAttribute("firstLevelCategories", firstLevelCategories);
                    request.setAttribute("secondLevelCategories", secondLevelCategories);
                    request.setAttribute("thirdLevelCategories", thirdLevelCategories);
                }
            }
        }

        request.setAttribute("goods",newBeeMallGoods);
        request.setAttribute("path", "good-edit");
        return "admin/newbee_mall_goods_edit";
    }

    @PostMapping("/goods/update")
    @ResponseBody
    public Result update(@RequestBody NewBeeMallGoods newBeeMallGoods){
        if (StringUtils.isEmpty(newBeeMallGoods.getGoodsName())
                || StringUtils.isEmpty(newBeeMallGoods.getGoodsIntro())
                || StringUtils.isEmpty(newBeeMallGoods.getTag())
                || Objects.isNull(newBeeMallGoods.getOriginalPrice())
                || Objects.isNull(newBeeMallGoods.getGoodsCategoryId())
                || Objects.isNull(newBeeMallGoods.getSellingPrice())
                || Objects.isNull(newBeeMallGoods.getStockNum())
                || Objects.isNull(newBeeMallGoods.getGoodsSellStatus())
                || StringUtils.isEmpty(newBeeMallGoods.getGoodsCoverImg())
                || StringUtils.isEmpty(newBeeMallGoods.getGoodsDetailContent())) {
            return ResultGenerator.genFailResult("参数异常！");
        }
        String result = newBeeMallGoodsService.updateNewBeeMallGoods(newBeeMallGoods);
        if(ServiceResultEnum.SUCCESS.getResult().equals(result)){
            return ResultGenerator.genSuccessResult();
        }else {
            return ResultGenerator.genFailResult(result);
        }

    }

    @GetMapping("/goods")
    public String goodsPage(HttpServletRequest request){
        request.setAttribute("path", "newbee_mall_goods");
        return "admin/newbee_mall_goods";
    }

    @RequestMapping(value = "/goods/list",method = RequestMethod.GET)
    @ResponseBody
    public Result list(@RequestParam Map<String,Object> param){
        if(StringUtils.isEmpty(param.get("page")) || StringUtils.isEmpty(param.get("limit"))){
            return ResultGenerator.genFailResult("参数异常");
        }

        PageQueryUtil pageQueryUtil = new PageQueryUtil(param);
        return ResultGenerator.genSuccessResult(newBeeMallGoodsService.getNewBeeMallGoodsPage(pageQueryUtil));

    }

    @RequestMapping(value = "/goods/status/{sellStatus}",method = RequestMethod.PUT)
    @ResponseBody
    public Result delete(@RequestBody Long[] ids,@PathVariable("sellStatus") int sellStatus){
        if(ids.length < 1){
            return ResultGenerator.genFailResult("参数异常");
        }

        if(sellStatus != Constants.SELL_STATUS_UP && sellStatus != Constants.SELL_STATUS_DOWN){
            return ResultGenerator.genFailResult("状态异常");
        }

        if(newBeeMallGoodsService.batchUpdateGoodsSellStatus(ids,sellStatus)>0){
            return ResultGenerator.genSuccessResult();
        }else {
            return ResultGenerator.genFailResult("修改失败");
        }
    }
}
