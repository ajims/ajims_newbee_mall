package ltd.newbee.mall.controller.admin;

import ltd.newbee.mall.entity.IndexConfig;
import ltd.newbee.mall.enums.ServiceResultEnum;
import ltd.newbee.mall.service.NewBeeMallIndexConfigService;
import ltd.newbee.mall.util.PageQueryUtil;
import ltd.newbee.mall.util.Result;
import ltd.newbee.mall.util.ResultGenerator;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import ltd.newbee.mall.enums.IndexConfigTypeEnum;
import org.thymeleaf.util.StringUtils;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.Map;
import java.util.Objects;

@Controller
@RequestMapping("/admin")
public class NewBeeMallGoodsIndexConfigController {

    @Resource
    private NewBeeMallIndexConfigService newBeeMallIndexConfigService;

    @GetMapping("/indexConfigs")
    public String indexConfigsPage(HttpServletRequest request, @RequestParam("configType") int config) {


        IndexConfigTypeEnum indexConfigTypeEnum = IndexConfigTypeEnum.getIndexConfigTypeEnumByType(config);

        //?
        request.setAttribute("path", indexConfigTypeEnum.getName());
        request.setAttribute("configType", config);

        return "admin/newbee_mall_index_config";
    }

    @GetMapping("/indexConfigs/list")
    @ResponseBody
    public Result list(@RequestParam Map<String, Object> params) {
        if (StringUtils.isEmpty((String) params.get("page")) || StringUtils.isEmpty((String) params.get("limit"))) {
            return ResultGenerator.genFailResult("参数异常！");
        }
        PageQueryUtil pageUtil = new PageQueryUtil(params);
        return ResultGenerator.genSuccessResult(newBeeMallIndexConfigService.getConfigsPage(pageUtil));

    }

    /*
    * 添加,@RequestBody 是指输入的一个整体是使用@RequestBody输入的
    * */
    @PostMapping("/indexConfigs/save")
    @ResponseBody
    public Result save(@RequestBody IndexConfig indexConfig){
        if(Objects.isNull(indexConfig.getConfigType())
            || StringUtils.isEmpty(indexConfig.getConfigName())
            || Objects.isNull(indexConfig.getConfigRank())){
                return ResultGenerator.genFailResult("参数异常");
        }
        String result = newBeeMallIndexConfigService.saveIndexConfig(indexConfig);
        if(ServiceResultEnum.SUCCESS.getResult().equals(result)){
            return ResultGenerator.genSuccessResult();
        }else {
            return ResultGenerator.genFailResult(result);
        }
    }

    /*
    * 修改
    * */
    @PostMapping("/indexConfigs/update")
    @ResponseBody
    public Result update(@RequestBody IndexConfig indexConfig){
        if(Objects.isNull(indexConfig.getConfigType())
                || StringUtils.isEmpty(indexConfig.getConfigName())
                || Objects.isNull(indexConfig.getConfigRank())){
            return ResultGenerator.genFailResult("参数异常");
        }
        String result = newBeeMallIndexConfigService.updateConfig(indexConfig);
        if(ServiceResultEnum.SUCCESS.getResult().equals(result)){
            return ResultGenerator.genSuccessResult();
        }else {
            return ResultGenerator.genFailResult(result);
        }

    }

    /*
    * 删除
    * */
    @PostMapping("/indexConfigs/delete")
    @ResponseBody
    public Result delete(@RequestBody Long[] ids){
        if (ids.length < 1){
            ResultGenerator.genFailResult("参数异常");
        }

        if(newBeeMallIndexConfigService.deleteBatch(ids)){
            return ResultGenerator.genSuccessResult();
        }else {
            return ResultGenerator.genFailResult("修改失败");
        }
    }

    /*
    * 详情
    * */
    @GetMapping("/indexConfigs/info/{id}")
    @ResponseBody
    public Result info(@PathVariable("id") Long id){
        IndexConfig indexConfig = newBeeMallIndexConfigService.getIndexConfigById(id);
        if (indexConfig == null){
            return ResultGenerator.genFailResult("未查询到数据");
        }
        return ResultGenerator.genSuccessResult();
    }
}
