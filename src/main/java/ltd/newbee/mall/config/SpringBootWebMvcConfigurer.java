package ltd.newbee.mall.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class SpringBootWebMvcConfigurer implements WebMvcConfigurer {

    public void addResourceHandlers(ResourceHandlerRegistry registry){
        //注意：路径前需要添加 file: 前缀
        registry.addResourceHandler("/goods-img/**")
                .addResourceLocations("file:D:\\upload\\goods-img\\");
    }

    /*
    * 通过该设置，所有以 /upload/ 开头的静态资源请求都会映射到 D 盘的 upload 目录下，
    * 与前面上传文件时设置目录类似，不同的系统比如 Linux 和 Windows，文件路径的写法不同。
    *
    * */
}
