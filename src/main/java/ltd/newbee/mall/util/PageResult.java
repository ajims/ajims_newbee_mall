package ltd.newbee.mall.util;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

//通过用于service层或者controller层
@Data
public class PageResult implements Serializable {

    //总记录数
    private int totalCount;

    //每页记录数
    private int pageSize;

    //总页数
    private int totalPage;

    //当前页数
    private int currentPage;

    //列表数据
    private List<?> list;

    /**
     * 分页
     *
     * @param list        列表数据
     * @param totalCount  总记录数
     * @param pageSize    每页记录数
     * @param currentPage 当前页数
     */
    public PageResult(int totalCount, int pageSize, int currentPage, List<?> list) {
        this.list = list;
        this.totalCount = totalCount;
        this.pageSize = pageSize;
        this.currentPage = currentPage;
        this.totalPage = (int) Math.ceil((double) totalCount / pageSize);
    }
}
